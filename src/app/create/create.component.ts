import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FdiService } from '../fdi.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public createForms!: FormGroup;
  // submitted: boolean = false;
  constructor(public fdiService:FdiService, public fb:FormBuilder ) {}
  
  // formControls = this.fdiService.form.controls;
  

  ngOnInit(): void {
    // console.log(this.fb)
    this.createForms = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit(){

  }
}
