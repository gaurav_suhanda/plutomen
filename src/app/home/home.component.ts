import { Component, OnInit } from '@angular/core';
import { FdiService } from '../fdi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public fdi: FdiService) { 
    console.log(this.fdi);
  }

  ngOnInit(): void {
  }

}
