import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, FormArray} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FdiService {
  public JSONPATH = 'http://apitest.pluto-men.com/api/';
  insertvar: any; 
  

  constructor(public myvar: HttpClient, public fb:FormBuilder) {
    console.log(this.myvar);
  }

  // form = new FormGroup ({
  //   $key: new FormControl(null),
  //   name: new FormControl('', Validators.required),
  //   email: new FormControl('', Validators.compose([Validators.required,Validators.email])),
  //   password: new FormControl('', Validators.required)
  // });

  // Some random function for reverse engineering..

  postData(jsonServerKey: string) {
    // console.log("Key..."+jsonServerKey);
    var finalpath = this.JSONPATH + jsonServerKey;
    //console.log(finalpath);
    return this.myvar.get(finalpath);
  }

  // insertData(insertvar) {
  //   this.JSONPATH.push({
  //     name: insertvar.name;
  //     email: insertvar.email;
  //     password: insertvar.password;
  //   });

  // }


  putData() {}

  deleteData() {}
}
