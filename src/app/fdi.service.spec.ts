import { TestBed } from '@angular/core/testing';

import { FdiService } from './fdi.service';

describe('FdiService', () => {
  let service: FdiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FdiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
