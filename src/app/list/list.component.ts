import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FdiService } from '../fdi.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  userData_json: any;
  

  constructor(public fdi: FdiService){}

  ngOnInit(): void {
    // this.myvar.get("http://localhost:3000/users").subscribe(
    //   (response)=>{
    //     console.log("Success!");
    //     console.log(response);
    //     this.userData_json = response
    //   },
    //   (error)=>{
    //     console.log("Error in API")
    //   }
    // );
    this.fdi.postData("user").subscribe(
      (response) =>{
        this.userData_json = response;
      }
    )
  }

}
